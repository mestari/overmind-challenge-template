import sc2
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *

class WorkerRushBot(sc2.BotAI):
    async def on_step(self, iteration):
        cc = self.units(COMMANDCENTER)
        depots = self.units(SUPPLYDEPOT)

        if not cc.exists:
            return
        else:
            cc = cc.first

        if self.supply_left < 3:
             if self.can_afford(SUPPLYDEPOT) and not self.already_pending(SUPPLYDEPOT):
                await self.build(SUPPLYDEPOT, near=cc.position.towards(self.game_info.map_center, 8))

        if self.can_afford(SCV) and self.workers.amount < 20 and cc.noqueue:
            await self.do(cc.train(SCV))


        if self.supply_used >= 15 and self.units(REFINERY).amount < 1:
            if self.can_afford(REFINERY):
                vgs = self.state.vespene_geyser.closer_than(20.0, cc)
                for vg in vgs:
                    if self.units(REFINERY).closer_than(1.0, vg).exists:
                        break

                    worker = self.select_build_worker(vg.position)
                    if worker is None:
                        break

                    await self.do(worker.build(REFINERY, vg))
                    break
        barracks_placement_position = self.main_base_ramp.barracks_correct_placement

        if depots.ready.exists and self.can_afford(BARRACKS) and not self.already_pending(BARRACKS):
            if self.units(BARRACKS).amount + self.already_pending(BARRACKS) > 0:
                return
            ws = self.workers.gathering
            if ws and barracks_placement_position: # if workers were found
                w = ws.random
                await self.do(w.build(BARRACKS, barracks_placement_position))


        for a in self.units(REFINERY):
            if a.assigned_harvesters < a.ideal_harvesters:
                w = self.workers.closer_than(20, a)
                if w.exists:
                    await self.do(w.random.gather(a))

        for scv in self.units(SCV).idle:
            await self.do(scv.gather(self.state.mineral_field.closest_to(cc)))


def main():
    run_game(maps.get("Simple96"), [
        Bot(Race.Terran, WorkerRushBot()),
        Computer(Race.Protoss, Difficulty.Medium)
    ], realtime=False)

if __name__ == '__main__':
    main()
